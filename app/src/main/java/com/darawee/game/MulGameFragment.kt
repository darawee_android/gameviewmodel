package com.darawee.game

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.darawee.game.databinding.FragmentMinusGameBinding
import com.darawee.game.databinding.FragmentMulGameBinding
import com.darawee.game.databinding.FragmentPlusBinding
import kotlinx.android.synthetic.main.fragment_minus_game.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MulGameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MulGameFragment : Fragment() {
    private lateinit var binding: FragmentMulGameBinding
    private lateinit var multiGameViewModel: MultiGameViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        multiGameViewModel = ViewModelProvider(this).get(MultiGameViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_mul_game, container, false)
        binding.multiGameViewModel  = multiGameViewModel
        multiGameViewModel.eventNextQuestion.observe(viewLifecycleOwner, Observer {hasEvent ->
            if(hasEvent) {
                enableAllButton()
                binding.invalidateAll()
                multiGameViewModel.onNextFinish()
            }

        })

        multiGameViewModel.eventHome.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_mulGameFragment_to_startMenuFragment)
                multiGameViewModel.changeToHomeFinish()
            }
        })

        multiGameViewModel.eventSelectChoice1.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                multiGameViewModel.onSelectChoice1Finish()
            }

        })

        multiGameViewModel.eventSelectChoice2.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                multiGameViewModel.onSelectChoice2Finish()
            }

        })

        multiGameViewModel.eventSelectChoice3.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                multiGameViewModel.onSelectChoice3Finish()
            }

        })


        return binding.root
    }
    fun disabledAllButton(){
        binding.apply {
            btnNum1.isEnabled =false
            btnNum2.isEnabled =false
            btnNum3.isEnabled =false
        }
    }
    fun enableAllButton(){
        binding.apply {
            btnNum1.isEnabled =true
            btnNum2.isEnabled =true
            btnNum3.isEnabled =true
        }
    }
}