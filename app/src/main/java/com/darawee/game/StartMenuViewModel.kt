package com.darawee.game

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class StartMenuViewModel : ViewModel() {
    private val _eventPlus = MutableLiveData<Boolean>()
    val eventPlus: LiveData<Boolean>
        get() = _eventPlus

    private val _eventMinus= MutableLiveData<Boolean>()
    val eventMinus: LiveData<Boolean>
        get() = _eventMinus

    private val _eventMulti = MutableLiveData<Boolean>()
    val eventMulti: LiveData<Boolean>
        get() = _eventMulti

    private val _score = MutableLiveData<Score>()
    val score: LiveData<Score>
        get() = _score

    fun changeToPlus(){
        Log.i("test","doakkoo")
        _eventPlus.value = true
    }
    fun changeToPlusFinish(){
        _eventPlus.value = false
    }

    fun changeToMinus(){
        _eventMinus.value = true
    }
    fun changeToMinusFinish(){
        _eventMinus.value = false
    }

    fun changeToMulti(){
        _eventMulti.value = true
    }
    fun changeToMultiFinish(){
        _eventMulti.value = false
    }

}